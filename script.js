    var path = window.location.pathname;
    var keywords;
    if (path.includes('electric-vehicles')) {
        keywords = ['cybertruck', 'tesla', 'rivian', 'nissan leaf', 'charging stations', 'electric vehicle']
    } else if (path.includes('solar')) {
        var keywords = ['solar', 'solar panels', 'community solar', 'posigen', 'sunpower', 'entergy solar']
    } 

    var serialize = function(obj) {
    var str = [];
    for (var p in obj)
        if (obj.hasOwnProperty(p)) {
        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        }
    return str.join("&");
    }

    var territories = ['US-LA', 'US-TX', 'US-MS', 'US-AR']
    var scripts = [];
    for (var k in keywords) {
        var keyword = keywords[k];
        $('#container').append(`
            <div class="flex-container">
                <h3>${keyword}</h3>
                <div class="row">
                    <div id="US-LA-${keyword}"></div>
                    <div id="US-AR-${keyword}"></div>
                    <div id="US-TX-${keyword}"></div>
                    <div id="US-MS-${keyword}"></div>
                </div>
            </div>
        `)
    }

    for (var i in territories) {
        var territory = territories[i];
        for (var k in keywords) {
            var default_timeframe = 'today 12-m';
            var keyword = keywords[k];
            var comparison_item = [{"keyword":keyword,"geo":territory,"time":default_timeframe}];
            trends.embed.renderExploreWidgetTo(document.getElementById(territory+'-'+keyword), "TIMESERIES", {
                "comparisonItem":comparison_item,
                "category":0,
                "property":""}, 
                {"exploreQuery":serialize(comparison_item[0]),
                "guestPath":"https://trends.google.com:443/trends/embed/"}
            )
        }
    }